import React, { useState } from "react";
import axios from "axios";
import { Button } from "react-bootstrap";

const FileUpload = () => {
  const [selectedFile, setSelectedFile] = useState(undefined);

  const changeFile = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const uploadFile = () => {
    const formData = new FormData();
    formData.append("file", selectedFile);
    const res = axios.post("/api/upload", formData);
  };

  return (
    <div>
      <input type="file" onChange={changeFile}></input>
      <Button onClick={uploadFile} size="sm">
        업로드
      </Button>
    </div>
  );
};

export default FileUpload;
