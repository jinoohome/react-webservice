import axios from "axios";
import React, { useEffect, useState } from "react";

function Child({ item, parentClick }) {
  const clickHandler = (e) => {
    alert("clicked!!");
  };

  // const { item, parentClick } = props/

  const inClickHandler = () => {
    return item;
  };

  return (
    <div>
      <button onClick={() => parentClick(item)}>click me </button>

      <div>
        list :<div>{item.col1}</div>
      </div>
    </div>
  );
}

const App = () => {
  const [list, setList] = useState([]);
  const [company, setCompany] = useState(false);

  // const copyList = this.state.list
  // this.setState({
  //   list : []``
  // })

  useEffect(() => {
    console.log("first");
    getList();
  }, []);

  const getList = () =>
    axios.get("/api/stock").then((res) => {
      setList(res.data);
      console.log("res", res.data);
    });

  const parentClickHandler = (value) => {
    alert("this is parent click handler!!" + value.col1);
  };

  return (
    <div>
      hello
      {list.map((item2, index) => (
        <Child item={item2} parentClick={parentClickHandler} />
      ))}
    </div>
  );
};

export default App;
