import React, { Component } from "react";

class PhoneForm extends Component {
  input = React.createRef;
  state = {
    name: "",
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onCreate(this.state);
    this.setState({
      name: "",
    });
    this.input.currnet.focus();
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          name="name"
          onChange={this.handleChange}
          value={this.state.name}
          ref={this.input}
        />
        <button type="submit">등록</button>
        <br />
      </form>
    );
  }
}

export default PhoneForm;
