import React, { useState, useEffect } from "react";
import "./App.css";
import axios from "axios";
import PhoneForm from "./PhoneForm";
import PhoneInfoList from "./PhoneInfoList";
import { Table } from "react-bootstrap";
import FileUpload from "./FileUpload";

//import Loader from "./Loader";

const App = () => {
  const [stocks, setStocks] = useState([]);

  useEffect(() => {
    axios.get("/api/stock").then((res) => {
      setStocks(res.data);
    });
  });

  return (
    <div>
      <FileUpload></FileUpload>

      <Table size="sm">
        <thead></thead>
        {stocks.map((stock) => (
          <tr>
            <td>{stock.col1}</td>
            <td>{stock.col2}</td>
            <td>{stock.col3}</td>
            <td>{stock.col4}</td>
            <td>{stock.col5}</td>
            <td>{stock.col6}</td>
            <td>{stock.col7}</td>
            <td>{stock.col8}</td>
            <td>{stock.col9}</td>
            <td>{stock.col10}</td>
          </tr>
        ))}
      </Table>
    </div>
  );
};

export default App;

// class App extends Component {
//   id = 0;

//   state = {
//     selectedFile: null,
//     information: [],
//     itemList: [],
//     keyword: "",
//   };
//   handleChange = (e) => {
//     this.setState({
//       keyword: e.target.value,
//     });
//   };

//   handleCreate = (data) => {
//     const { information } = this.state;
//     this.setState({
//       information: information.concat({
//         ...data,
//         id: this.id++,
//       }),
//     });
//   };

//   handleFileInput = (e) => {
//     this.setState({
//       selectedFile: e.target.files[0],
//     });
//   };

//   handleRemove = (id) => {
//     const { information } = this.state;
//     this.setState({
//       information: information.filter((info) => info.id !== id),
//     });
//   };

//   handleUpdate = (id, data) => {
//     const { information } = this.state;

//     this.setState({
//       information: information.map((info) => {
//         if (info.id === id) {
//           return {
//             id,
//             ...data,
//           };
//         }
//         return info;
//       }),
//     });
//   };

//   uploadBtn = async () => {
//     try {
//       const formData = new FormData();
//       formData.append("file", this.state.selectedFile);
//       const response = await axios
//         .post("/api/upload", formData)
//         .then(function(response) {
//           //seccess
//           console.log("seccess");
//           window.location.reload();
//         })
//         .catch(function(error) {
//           //error
//           console.log("error");
//         })
//         .then(function() {
//           //finally
//           console.log("finally");
//         });

//       console.log(response);
//     } catch (e) {
//       console.log(e);
//     }
//   };

//   async componentDidMount() {
//     let { itemList } = await axios.get("/api/stock");
//     this.setState({ itemList: this.stateitemList });
//     console.log(itemList);
//   }

//   render() {
//     return (
//       <form>
//         <div className="App">
//           <h2>엑셀 업로드 리스트</h2>
//           <PhoneForm onCreate={this.handleCreate} />
//           <br />
//           <input
//             value={this.state.keyword}
//             onChange={this.handleChange}
//             placeholder="검색..."
//           />
//           <br />
//           <PhoneInfoList
//             data={this.state.information.filter(
//               (info) => info.name.indexOf(this.state.keyword) > -1
//             )}
//             onRemove={this.handleRemove}
//             onUpdate={this.handleUpdate}
//           />

//           <br />
//           <input
//             type="file"
//             name="file"
//             onChange={(e) => this.handleFileInput(e)}
//           />

//           <button type="button" onClick={() => this.uploadBtn()}>
//             업로드
//           </button>
//         </div>
//       </form>
//     );
//   }
// }

//export default App;
